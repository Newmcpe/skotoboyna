package ru.newmcpe.skotoboyna.tasks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import ru.newmcpe.skotoboyna.Main;
import ru.newmcpe.skotoboyna.utils.Utils;

import java.util.List;

public class SpawnMobsTask implements Runnable {
    private final int mobSpawnOffset = 14;
    private final double ENTITY_MAX_HEALTH = 20;
    private Location mobSpawnLoc;

    public SpawnMobsTask() {
        mobSpawnLoc = new Location(Bukkit.getWorld("world"), 157, 88, 259);
    }

    @Override
    public void run() {
        double newX = mobSpawnLoc.getX() + Utils.getRandomNumberInRange(-mobSpawnOffset, mobSpawnOffset);
        double newZ = mobSpawnLoc.getZ() + Utils.getRandomNumberInRange(-mobSpawnOffset, mobSpawnOffset);
        Location finalMobSpawnLoc = new Location(mobSpawnLoc.getWorld(), newX, mobSpawnLoc.getY(), newZ);

        List<String> mobsInPaddock = Main.instance.getMobsInPaddock();
        EntityType randomEntityType = EntityType.valueOf(mobsInPaddock.get(Utils.getRandomNumberInRange(0, mobsInPaddock.size() - 1)));

        LivingEntity entity = (LivingEntity) finalMobSpawnLoc.getWorld().spawnEntity(finalMobSpawnLoc, randomEntityType);
        entity.setCanPickupItems(false);
        entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(ENTITY_MAX_HEALTH);
        entity.setHealth(ENTITY_MAX_HEALTH);
        entity.setCollidable(false);
        entity.setCustomName(entity.getName());
        entity.setCustomNameVisible(true);
    }
}
