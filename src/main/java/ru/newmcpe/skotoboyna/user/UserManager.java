package ru.newmcpe.skotoboyna.user;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class UserManager {
    private static UserManager instance = new UserManager();
    private Map<UUID, User> users;
    public UserManager() {
        users = new HashMap<>();
    }

    public static UserManager getInstance() {
        return instance;
    }

    public Map<UUID, User> getUsers() {
        return users;
    }

    public void addUser(UUID uuid) {
        User user = new User(uuid);
        users.put(uuid, user);

        reloadScoreBoard(user);
    }

    public void removeUser(UUID uuid) {
        users.remove(uuid);
    }

    public User getUser(UUID uuid) {
        return users.get(uuid);
    }

    public void reloadScoreBoard(User user) {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Objective o = scoreboard.registerNewObjective("title", "dummy");
        o.setDisplayName(ChatColor.GREEN + "Скотобойня");
        o.setDisplaySlot(DisplaySlot.SIDEBAR);

        Score xpScore = o.getScore("Опыт: §e" + user.getXp());
        xpScore.setScore(2);
        Score levelScore = o.getScore("Уровень: §e" + user.getLevel());
        levelScore.setScore(4);
        Score coinsScore = o.getScore("Монеты: §e" + user.getCoins());
        coinsScore.setScore(6);

        user.getPlayer().setScoreboard(scoreboard);
    }
}
