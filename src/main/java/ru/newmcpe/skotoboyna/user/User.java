package ru.newmcpe.skotoboyna.user;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jooq.Record;
import ru.newmcpe.skotoboyna.mysql.SQL;

import java.util.UUID;

@Getter
public class User {
    private UUID UUID;
    private int xp;
    private int level;
    private int coins;

    public User(UUID UUID) {
        this.UUID = UUID;
        try {
            SQL.asyncPoll(create -> {
                Record record = create.fetchOne("SELECT * FROM `skotoboyna_users` WHERE `UUID`=?", UUID.toString());
                if (record == null) {
                    this.createBaseUser(UUID);
                    record = create.fetchOne("SELECT * FROM `skotoboyna_users` WHERE `UUID`=?", UUID.toString());
                }
                xp = record.get("xp", int.class);
                level = record.get("level", int.class);
                coins = record.get("coins", int.class);
            }).get();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void createBaseUser(UUID uuid) {
        try {
            SQL.asyncPoll(create -> {
                create.execute("INSERT INTO `skotoboyna_users` (`UUID`,`xp`,`level`,`coins`) VALUES(?,?,?,?)", uuid.toString(), 1, 1, 0);
            }).get();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public void addExp(int exp) {
        SQL.asyncPoll(create -> create.execute("UPDATE `skotoboyna_users` SET `xp`=`xp`+? WHERE `uuid`=?", exp, this.getUUID()));
        this.xp = this.xp + exp;

        if (this.canUpLevel()) {
            addLevel();
        }

        UserManager.getInstance().reloadScoreBoard(this);
    }

    public void addCoins(int coins) {
        SQL.asyncPoll(create -> create.execute("UPDATE `skotoboyna_users` SET `coins`=`coins`+? WHERE `uuid`=?", coins, this.getUUID()));
        this.coins = this.coins + coins;

        UserManager.getInstance().reloadScoreBoard(this);
    }

    public void takeCoins(int coins) {
        SQL.asyncPoll(create -> create.execute("UPDATE `skotoboyna_users` SET `coins`=`coins`-? WHERE `uuid`=?", coins, this.getUUID()));
        this.coins = this.coins - coins;

        UserManager.getInstance().reloadScoreBoard(this);
    }

    private void addLevel() {
        SQL.asyncPoll(create -> create.execute("UPDATE `skotoboyna_users` SET `level`=`level`+? WHERE `uuid`=?", 1, this.getUUID()));
        this.level = this.level + 1;

        this.getPlayer().sendMessage("§aПоздравляем, ваш уровень был повышен до " + this.getLevel());

        UserManager.getInstance().reloadScoreBoard(this);
    }

    private boolean canUpLevel() {
        int baseXpToUpLevel = 5;

        return this.getXp() >= (baseXpToUpLevel * this.getLevel());
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(this.getUUID());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof User)) throw new IllegalArgumentException("Аргументом может быть только User");
        return ((User) obj).getUUID() == this.getUUID();
    }
}

