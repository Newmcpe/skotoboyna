package ru.newmcpe.skotoboyna.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.newmcpe.skotoboyna.Main;
import ru.newmcpe.skotoboyna.shop.Shipment;
import ru.newmcpe.skotoboyna.shop.ShopManager;
import ru.newmcpe.skotoboyna.user.User;
import ru.newmcpe.skotoboyna.user.UserManager;

import java.util.Arrays;

public class EventListener implements Listener {
    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        UserManager.getInstance().addUser(player.getUniqueId());
    }

    @EventHandler
    public void onLeft(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        UserManager.getInstance().removeUser(player.getUniqueId());
    }

    @EventHandler
    public void onDeath(EntityDeathEvent event) {
        Entity deadEntity = event.getEntity();

        if (deadEntity.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent damageCause = (EntityDamageByEntityEvent) deadEntity.getLastDamageCause();
            Entity killerEntity = damageCause.getDamager();
            if (killerEntity instanceof Player) {
                if (Arrays.asList(Main.instance.getMobsInPaddock()).contains(deadEntity.getType().name())) {
                    User user = UserManager.getInstance().getUser(killerEntity.getUniqueId());
                    user.addExp(2);
                    user.addCoins(10);

                    user.getPlayer().sendActionBar("+§a§l10$");

                    event.getDrops().clear();
                }
            }
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        Inventory inventory = event.getClickedInventory();
        ItemStack clickedItem = event.getCurrentItem();
        if (ChatColor.stripColor(inventory.getName()).equalsIgnoreCase("Магазин")) {
            Shipment shipment = ShopManager.getInstance().getShipmentByType(clickedItem.getType());
            if (shipment != null) {
                User user = UserManager.getInstance().getUser(player.getUniqueId());
                if (user.getLevel() >= shipment.getMinLevel()) {
                    if (user.getCoins() >= shipment.getPrice()) {
                        player.getInventory().addItem(new ItemStack(shipment.getType()));
                        user.takeCoins(shipment.getPrice());
                        player.sendMessage("§aПредмет успешно куплен!");
                    } else {
                        player.sendMessage("§cНе хватает денег!");
                    }
                } else {
                    player.sendMessage("§cМинимальный уровень предмета: " + shipment.getMinLevel());
                }
            }
            event.setCancelled(true);
        }
    }
}
