package ru.newmcpe.skotoboyna.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import ru.newmcpe.skotoboyna.shop.Shipment;
import ru.newmcpe.skotoboyna.shop.ShopManager;

import java.util.ArrayList;
import java.util.List;

public class ShopCommand extends Command {
    public ShopCommand() {
        super("shop");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        Player player = (Player) commandSender;
        Inventory gui = Bukkit.createInventory(null, 36, "§aМагазин");

        for (Shipment shipment : ShopManager.getInstance().getShipments()) {
            ItemStack itemStack = new ItemStack(shipment.getType());
            ItemMeta itemMeta = itemStack.getItemMeta().clone();

            List<String> lore = new ArrayList<>();
            lore.add("§aЦена: " + shipment.getPrice());
            lore.add("§aМинимальный уровень: " + shipment.getMinLevel());

            itemMeta.setLore(lore);
            itemStack.setItemMeta(itemMeta);

            gui.addItem(itemStack);
        }

        player.openInventory(gui);
        return true;
    }
}
