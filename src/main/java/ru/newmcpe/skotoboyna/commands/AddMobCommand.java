package ru.newmcpe.skotoboyna.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.newmcpe.skotoboyna.Main;

import java.util.List;

public class AddMobCommand extends Command {

    public AddMobCommand() {
        super("addmob");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        Player player = (Player) commandSender;
        String mob = strings[0];
        if (player.hasPermission("skotoboyna.addmobs")) {
            List<String> mobsinpaddock = Main.instance.getMobsInPaddock();
            mobsinpaddock.add(mob);
            Main.instance.getConfig().setAndSave("mobsinpaddock", mobsinpaddock);
            player.sendMessage("§aМоб " + mob + " успешно добавлен на скотобойню!");
        }
        return true;
    }
}
