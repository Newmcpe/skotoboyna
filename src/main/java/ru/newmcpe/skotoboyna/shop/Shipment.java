package ru.newmcpe.skotoboyna.shop;

import lombok.Getter;
import org.bukkit.Material;

@Getter
public class Shipment {
    private Material type;
    private int price;
    private int minLevel;

    public Shipment(Material type, int price, int minLevel) {
        this.type = type;
        this.price = price;
        this.minLevel = minLevel;
    }
}
