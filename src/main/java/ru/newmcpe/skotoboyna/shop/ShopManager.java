package ru.newmcpe.skotoboyna.shop;

import lombok.Getter;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.Objects;

@Getter
public class ShopManager {
    private ArrayList<Shipment> shipments;
    private static ShopManager instance = new ShopManager();

    public ShopManager() {
        shipments = new ArrayList<>();

        initializeShipments();
    }

    private void initializeShipments() {
        shipments.add(new Shipment(Material.WOOD_SWORD, 25, 5));
        shipments.add(new Shipment(Material.STONE_SWORD, 50, 10));
        shipments.add(new Shipment(Material.IRON_SWORD, 75, 15));
        shipments.add(new Shipment(Material.DIAMOND_SWORD, 100, 20));
    }

    public Shipment getShipmentByType(Material material) {
        return shipments
                .stream()
                .filter(shipment -> Objects.equals(shipment.getType(), material))
                .findAny()
                .orElse(null);
    }

    public static ShopManager getInstance() {
        return instance;
    }
}
