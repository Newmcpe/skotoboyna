package ru.newmcpe.skotoboyna.mysql;

import org.jooq.DSLContext;

import java.util.Arrays;
import java.util.concurrent.Future;
import java.util.function.Consumer;

public class SQL {

    MySQL mySQL = MySQL.getInstance();

    /**
     * Получить соединение с БД
     *
     * @return соединение
     */
    public static DSLContext getCreate() {
        return MySQL.getCreate();
    }


    /**
     * Асинхронный еденичный запрос<br>
     * <b>Внимание, работа asyncPoll отличается от async в Bukkit, тут задействован пул потоков</b>
     *
     * @param query    запрос
     * @param bindings параметры
     */
    public static void asyncPoll(String query, Object... bindings) {
        MySQL.getServicePoll().submit(() -> {
            printTime(query + " (params: " + Arrays.toString(bindings) + ")", () -> {
                try {
                    MySQL.getCreate().execute(query, bindings);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        });
    }

    /**
     * Асинхронный доступ к JOOQ - иструменту, который позволяет работать с БД<br>
     * <b>Внимание, работа asyncPoll отличается от async в Bukkit, тут задействован пул потоков</b>
     *
     * @param create JOOQ
     */
    public static Future<?> asyncPoll(SQLConsumer create) {
        return MySQL.getServicePoll().submit(() -> {
            printTime(create, () -> {
                try {
                    create.run(MySQL.getCreate());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        });
    }

    /**
     * Асинхронный доступ к JOOQ - иструменту, который позволяет работать с БД<br>
     * <b>Внимание, работа asyncPoll отличается от async в Bukkit, тут задействован пул потоков</b>
     *
     * @param create JOOQ
     * @param result обработка результата в синхронном потоке
     */
    public static <R> Future<?> asyncPoll(SQLFunction<R> create, Consumer<R> result) {
        return MySQL.getServicePoll().submit(() ->
                printTime(create, () ->
                        Try.ignore(() -> {
                            R apply = create.apply(MySQL.getCreate());
                            result.accept(apply);
                        })
                )
        );
    }

    public static void sync(String sql, Object... bindings) {
        printTime(sql, () -> getCreate().execute(sql, bindings));
    }

    private static void printTime(Object print, Runnable runnable) {
        long start = System.currentTimeMillis();
        try {
            runnable.run();
        } finally {
            long after = System.currentTimeMillis() - start;
            if (after > 500) {
                System.out.println("SQL задержка, объект " + print + " выполнялся " + after + "ms.");
            }
        }
    }
}
