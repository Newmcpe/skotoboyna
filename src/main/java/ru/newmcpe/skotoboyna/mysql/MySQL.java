package ru.newmcpe.skotoboyna.mysql;

import com.p6spy.engine.spy.P6DataSource;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MySQL {

    private static MySQL instance;
    private static ExecutorService servicePoll;
    private static DSLContext create;
    private HikariDataSource hikariDataSource;

    public MySQL() {
        instance = this;
        System.out.println("База данных запускается...");
    }

    public static MySQL getInstance() {
        return instance;
    }

    public static ExecutorService getServicePoll() {
        return servicePoll;
    }

    public static DSLContext getCreate() {
        return create;
    }

    public void start() {
        initExecutorService();
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:mysql://localhost:3306/Server");
        config.setUsername("root");
        config.setPassword("artemcfki");
        config.setDriverClassName("com.mysql.jdbc.Driver");

        config.setMaximumPoolSize(15 + 2); // сколько потоков, столько и соединений + 2 на всякий пожарный

        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.addDataSourceProperty("characterEncoding", "utf8");
        config.addDataSourceProperty("useUnicode", "true");
        config.addDataSourceProperty("useSSL", "false");
        config.addDataSourceProperty("useJDBCCompliantTimezoneShift", "true");
        config.addDataSourceProperty("useLegacyDatetimeCode", "false");
        config.addDataSourceProperty("serverTimezone", TimeZone.getDefault().getID());

        try {
            hikariDataSource = new HikariDataSource(config);

            // логирование запросов
            boolean logQuery = true;

            this.create = DSL.using(logQuery ? new P6DataSource(hikariDataSource) : hikariDataSource, SQLDialect.MYSQL);
        } catch (Exception e) {
            throw new RuntimeException("СОЕДИНЕНИЕ С БД НЕ УСТАНОВЛЕНО! ПИЗДЕЦ!", e);
        }
    }

    private void initExecutorService() {
        servicePoll = Executors.newFixedThreadPool(15);
    }
}
