package ru.newmcpe.skotoboyna.mysql;

import org.jooq.DSLContext;

import java.sql.SQLException;

public interface SQLFunction<R> {

    R apply(DSLContext create) throws SQLException;
}
