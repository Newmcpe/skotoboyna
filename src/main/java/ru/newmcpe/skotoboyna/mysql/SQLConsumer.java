package ru.newmcpe.skotoboyna.mysql;

import org.jooq.DSLContext;

import java.sql.SQLException;

public interface SQLConsumer {

    void run(DSLContext create) throws SQLException;
}
