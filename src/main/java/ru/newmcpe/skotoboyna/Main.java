package ru.newmcpe.skotoboyna;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.plugin.java.JavaPlugin;
import ru.newmcpe.skotoboyna.commands.AddMobCommand;
import ru.newmcpe.skotoboyna.commands.ShopCommand;
import ru.newmcpe.skotoboyna.listeners.EventListener;
import ru.newmcpe.skotoboyna.mysql.MySQL;
import ru.newmcpe.skotoboyna.schedule.Schedule;
import ru.newmcpe.skotoboyna.tasks.SpawnMobsTask;
import ru.newmcpe.skotoboyna.utils.Config;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main extends JavaPlugin {

    public static Main instance;
    private Config config;

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new EventListener(), this);
        initialize();
    }

    private void startMobSpawnTask() {
        Schedule.timer(new SpawnMobsTask(), 0, 10, TimeUnit.SECONDS);
    }

    private void initialize() {
        instance = this;
        config = new Config(this, "config.yml");

        MySQL mySQL = new MySQL();
        mySQL.start();

        startMobSpawnTask();

        CommandMap commandMap = Bukkit.getCommandMap();
        commandMap.register("newmcpe", new AddMobCommand());
        commandMap.register("newmcpe", new ShopCommand());
    }

    public Config getConfig() {
        return config;
    }

    public List<String> getMobsInPaddock() {
        return config.getOrSet("mobsinpaddock", Arrays.asList("SHEEP", "COW", "PIG"));
    }
}