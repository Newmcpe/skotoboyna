package ru.newmcpe.skotoboyna.utils;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;

public class Config extends YamlConfiguration {

    public static final String SEP = File.separator; // separator - длинное слово!!
    protected String description = "";
    private File file;
    private boolean first = false;

    /**
     * Загрузить конфиг со строки
     *
     * @param data дата конфига
     */
    public Config(String data) {
        try {
            this.loadFromString(data);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Загрузить конфиг
     *
     * @param file файл конфига
     */
    public Config(File file, boolean load) {
        this.file = file;
        if (load) {
            this.reload();
        }
    }

    public Config(File file) {
        this(file, true);
    }

    /**
     * Загрузить конфиг
     *
     * @param plugin плагин
     * @param path   путь, относительно директории плагина
     */
    public Config(Plugin plugin, String path) {
        this.file = new File(plugin.getDataFolder() + SEP + path);
        this.reload();
    }

    public void save() {
        if (file != null) {
            try {
                FileOutputStream stream = new FileOutputStream(file);
                stream.write(this.saveDataToString().getBytes());
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected String saveDataToString() {
        StringBuilder builder = new StringBuilder();
        if (!description.isEmpty()) {
            builder.append('#').append(description.replace("\n", "\n#")).append("\n");
        }
        builder.append(this.saveToString());
        return builder.toString();
    }

    public void reload() {
        if (file == null) {
            throw new NullPointerException("конфиг не был загружен с файла, сохранить его нельзя");
        }
        this.createFile();
        this.loadFromFile();
    }

    protected void loadFromFile() {
        try {
            String content = this.loadDataFromFile();
            StringBuilder dataBuild = new StringBuilder();
            StringBuilder descBuild = new StringBuilder();
            for (String line : content.split("\n")) {
                if (!line.isEmpty()) {
                    if (line.charAt(0) == '#') {
                        descBuild.append(line.substring(1)).append("\n");
                    } else {
                        dataBuild.append(line).append("\n");
                    }
                }
            }
            this.description = StringUtil.removeLast(descBuild.toString(), 1);
            this.loadFromString(dataBuild.toString());
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    protected String loadDataFromFile() throws IOException {
        FileInputStream stream = new FileInputStream(file);
        byte[] bytes = new byte[stream.available()];
        stream.read(bytes);
        stream.close();
        return new String(bytes);
    }

    protected void createFile() {
        if (!file.exists()) {
            try {
                first = true;
                file.getParentFile().mkdirs();
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            first = false;
        }
    }

    public String getDescription() {
        return description;
    }

    /**
     * Вставить комментарий кофига<br>
     * Он будет виден в самом верху конфига<br>
     *
     * @param description строка с описанием, можно использовать переносы - '\n'
     */
    public void setDescription(String description) {
        if (!this.description.equals(description)) {
            this.description = description;
            this.save();
        }
    }

    /**
     * Получить или вставить новое значение в конфиг
     *
     * @param path путь
     * @param def  значение по уполчанию
     * @return значние из конфига или значение по умолчанию
     */
    @SuppressWarnings("unchecked")
    public <T> T getOrSet(String path, T def) {
        if (!this.contains(path)) {
            this.setAndSave(path, def);
            return def;
        } else {
            return (T) this.get(path);
        }
    }

    /**
     * Получить или вставить новое значение в конфиг типа <code>? extends Number</code><br>
     * Добавлено в связи с тем, что часто возникают прроблемы с ClassCastException (int -> long, double - float)
     *
     * @param path путь
     * @param def  значение по уполчанию
     * @return значние их конфига или значение по умолчанию
     */
    public Number getOrSetNumber(String path, Number def) {
        if (!this.contains(path)) {
            this.setAndSave(path, def);
            return def;
        } else {
            return ((Number) this.get(path));
        }
    }

    /**
     * Установить значение, если его не существует
     *
     * @param path
     * @param value
     */
    public void setIfNotExist(String path, Object value) {
        if (!this.contains(path)) {
            this.setAndSave(path, value);
        }
    }

    public void setDefault(String path, Object value) {
        if (first) {
            this.setAndSave(path, value);
        }
    }

    /**
     * Редактировать и сохранить конфиг
     *
     * @param path
     * @param value
     */
    public void setAndSave(String path, Object value) {
        this.set(path, value);
        this.save();
    }

    @Override
    public void set(String path, Object value) {
        super.set(path, value);
    }

    /**
     * Создать секцию, если ее нету, она будет пустой
     *
     * @param path
     */
    public void createSectionIfNotExist(String path) {
        //я даун
        if (!this.contains(path)) {
            this.setAndSave(path + ".lkjsdafknhsdjklhfalsjhfwui3324ij2i3j4", 10);
            this.setAndSave(path + ".lkjsdafknhsdjklhfalsjhfwui3324ij2i3j4", null);
        }
    }

    /**
     * @deprecated используйте {@link #getKeys(String)}
     */
    @Override
    @Deprecated
    public ConfigurationSection getConfigurationSection(String path) {
        return super.getConfigurationSection(path);
    }

    /**
     * Генерировать свободный путь
     *
     * @param path путь, к которому будем генерировать новую секцию
     */
    public int generateNumberPath(String path) {
        for (int i = 0; ; i++) {
            if (!this.contains(path + "." + i)) {
                return i;
            }
        }
    }

    public float getFloat(String s) {
        return (float) this.getDouble(s);
    }

    /**
     * Получить все параметры в указанной секции
     *
     * @param section путь к секции
     * @return список всех параметров из этой секции
     */
    public Set<String> getKeys(String section) {
        this.createSectionIfNotExist(section);
        return this.getConfigurationSection(section).getKeys(false);
    }

    public File getFile() {
        return file;
    }

    /**
     * Установить файл, куда сохранять конфиг
     *
     * @param file файл конфига
     */
    public void setFile(File file) {
        this.file = file;
    }

    public boolean isFirst() {
        return first;
    }
}
